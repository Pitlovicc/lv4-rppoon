﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5.zadataj
{
    class DiscountedItem : RentableDecorator
    {

        private double Percentage;
        public DiscountedItem(IRentable rentable, double Percentage) : base(rentable)
        {
            this.Percentage = Percentage;
        }
        public override double CalculatePrice()
        {
            double x;
            x = base.CalculatePrice() * (0.01 * this.Percentage);
            Console.WriteLine(base.CalculatePrice() - x);
            return (base.CalculatePrice() - x);
        }

        public override String Description
        {
            get
            {
                return "Now at " + this.Percentage + " % off " + base.Description;
            }
        }
    }
}
