﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1.zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset dataset = new Dataset(@"E:\2. godina ferit\4. semestar\RPOON\LV4\CSV.txt");
            Analyzer3rdParty analyzer = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analyzer);


            //prosjek redova
            Console.WriteLine("Prosjek redova:");
            double[] AveragePerRow = adapter.CalculateAveragePerRow(dataset);
            foreach (double number in AveragePerRow)
            {
                Console.WriteLine(number);
            }

            //prosjek stupaca
            Console.WriteLine("Prosjek stupaca:");
            double[] AveragePerColumn = adapter.CalculateAveragePerColumn(dataset);
            foreach (double number in AveragePerColumn)
            {
                Console.WriteLine(number);
            }

        }
    }
}
