﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4.zadatak
{
    class Book : IRentable
    {
        private readonly double BaseBookPrice = 3.99;
        public String Title { get; private set; }

        public Book(String Title)
        {
            this.Title = Title;
        }
        public string Description { get { return this.Title; } }

        public double CalculatePrice()
        {
            return BaseBookPrice;
        }
    }
}
