﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4.zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> BookandVideo = new List<IRentable>();
            Video video = new Video("Umri muški");
            Book book = new Book("Judita");
            RentingConsolePrinter print = new RentingConsolePrinter();

            BookandVideo.Add(video);
            BookandVideo.Add(book);
            print.PrintTotalPrice(BookandVideo);
            print.DisplayItems(BookandVideo);

            Console.WriteLine();
            Console.WriteLine("Nakon dodavanja knjige i filma:");
            Console.WriteLine();
            HotItem hotItem = new HotItem(new Book("Harry Potter"));
            HotItem hotItem1 = new HotItem(new Video("Titanic"));

            BookandVideo.Add(hotItem);
            BookandVideo.Add(hotItem1);

            print.PrintTotalPrice(BookandVideo);
            print.DisplayItems(BookandVideo);


        }
    }
}
