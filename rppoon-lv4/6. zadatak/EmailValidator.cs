﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6.zadatak
{
    class EmailValidator : IEmailValidatorService
    {
        public bool IsValidAddress(string candidate)
        {
            if (String.IsNullOrEmpty(candidate))
            {
                return false;
            }
            return ContainsAtSign(candidate) && CointainsCorrectEnd(candidate);
        }
        private bool ContainsAtSign(string candidate)
        {
            bool hasAtSign = false;
            if (candidate.Contains('@')) hasAtSign = true;
            return (hasAtSign);
        }

        private bool CointainsCorrectEnd(string candidate)
        {

            bool endsWithHr = false;
            bool endsWithCom = false;
            if (candidate.EndsWith(".com")) endsWithCom = true;
            if (candidate.EndsWith(".hr")) endsWithHr = true;
            return (endsWithHr || endsWithCom);

        }
    }
}
