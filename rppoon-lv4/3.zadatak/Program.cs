﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3.zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> BookandVideo = new List<IRentable>();
            Video video = new Video("Umri muški");
            Book book = new Book("Judita");

            BookandVideo.Add(video);
            BookandVideo.Add(book);

            RentingConsolePrinter print = new RentingConsolePrinter();
            print.PrintTotalPrice(BookandVideo);
            print.DisplayItems(BookandVideo);
        }
    }
}
