﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7.zadatak
{
    interface IEmailValidatorService
    {
        bool IsValidAddress(String candidate);
    }
}
