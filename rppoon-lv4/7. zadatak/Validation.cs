﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7.zadatak
{

    class Validation : IRegistrationValidator
    {
        EmailValidator email = new EmailValidator();
        PasswordValidator password = new PasswordValidator(5);

        //public Validation()
        //{
        //    this.email = email;
        //    this.password = password;
        //}

        public bool IsUserEntryValid(UserEntry entry)
        {

            if (email.IsValidAddress(entry.Email) && password.IsValidPassword(entry.Password))
            {
                return true;
            }
            else
            {
                return IsUserEntryValid(entry);
            }
           
        }  
     }
}


